# seL4 kernel build and configuration

This repository helps with building compatible configurations of the seL4
kernel for Robigalia. Its structure is similar to sel4test, but doesn't
contain any of the userspace C libraries.

The submodules here should be kept in careful sync with sel4-sys, especially
the kernel submodule.

# Building the kernel

Look at the `configs` directory, and run `make $PLAT_defconfig`. For example,
`make x64_qemu_defconfig`.

You can then run `make` and the kernel will be built into `build/kernel`.

# Configuring the kernel

By default, debug printing is enabled for the kernel.

You can tweak the kernel config with `make menuconfig`. Be warned that some
settings may break the Rust userspace support! In particular, for x64:

- `XSAVE_SIZE` of less than 576 will break creating TCBs
- `SYSCALL=n` and `SYSENTER=y` will break all syscalls.
- `IOMMU=y`, `VTX=y`, `HUGE_PAGE=y`  will probably break object type numbering, and thus
   creating new objects.

For ARM: we don't support ARM yet.

# Platforms

Currently supported platforms are x64 "PC99".

There is a config for x86 PC99, but it is completely untested, since we don't
support 32-bit at all anymore.
